<?php

/**
 * The template for displaying the footer
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WDD
 */

?>


<footer id="colophon" class="site-footer">

	<div class="f-wrap">
		<div class="footer-description left">
			<ul>
				<li>
					<h3>About us</h3>
				</li>
				<li>
					<p>Tumble Bus parties are 60 minutes in length and is ideal for kids 3-5 years of age.</p>
				</li>
			</ul>
			<ul>
				<li>
					<h3>Quick Links</h3>
				</li>
				<?php
				wp_nav_menu(array(
					'container'      => null,
					'items_wrap'     => '%3$s',
					'theme_location' => 'menu-2',
					'menu_id'        => 'footer-menu',
				));
				?>
			</ul>

			<p class="wdd">Create with <i class="fas fa-heart pulsation"></i> by <a target="_blank" href="http://waydeepdigital.com">Way Deep Digital</a> team</p>
		</div>



		<div class="footer-description right">
			<ul>
				<li>
					<h3>Connect</h3>
				</li>
				<li>
					<p><b>Dennis Carlson: <a href="tel:503-234-1883">503-234-1883</a></b></p>
				</li>
				<li class="social">
					<a target="_blank" href="<?php echo esc_url(get_field('facebook', 11)); ?>"><i class="fab fa-facebook"></i></a>
					<a target="_blank" href="<?php echo esc_url(get_field('instagram', 11)); ?>"><i class="fab fa-instagram"></i></a>
				</li>

			</ul>
			<!-- <ul>
				<li>
					<p><h3>Newsletter</h3></p>
				</li>
				<li class="newsletter">
					<input type="email" placeholder="Email">
					<a href="" class="btn-purple"><span>Send</span></a>
				</li>
			</ul> -->
			<p class="copyright">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tumblebus_pdx-white.png" alt="logo">
				<span><?php _e(date('Y')); ?></span>
			</p>
		</div>
	</div>
</footer><!-- #colophon -->

</div><!-- #site -->




<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script> -->

<script>
	// $(document).ready(() => {
	// 	$('.tabs-nav a').click(function(e) {
	// 		e.preventDefault();

	// 		$('.tab').removeClass('active');
	// 		$('.tabs-nav li').removeClass('active');

	// 		$(this).parent('li').addClass('active');

	// 		const href = $(this).attr('href');
	// 		$(href).addClass('active');
	// 	})
	// })
</script>

<?php wp_footer(); ?>

</body>

</html>