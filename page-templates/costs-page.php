<?php /* Template Name: The Cost page */
get_header();
?>

<div id="primary" class="content-area">

	<section class="orange-title">
		<h1><?php echo get_field('the_costs_page_header_first'); ?></h1>
	</section>

	<section class="blocks-wrap">
		<div class="block">
			<p class="title"><?php echo get_field('the_first_inner_block_header'); ?></p>
			<p class="text"><?php echo get_field('the_first_inner_block_description'); ?></p>
		</div>
		<div class="block">
			<p class="title"><?php echo get_field('the_second_inner_block_header'); ?></p>
			<p class="text"><?php echo get_field('the_second_inner_block_description'); ?></p>
		</div>
		<div class="block">
			<p class="title"><?php echo get_field('the_third_inner_block_header'); ?></p>
			<p class="text"><?php echo get_field('the_third_inner_block_description'); ?></p>
		</div>
	</section>

	<section class="blue-title">
		<h2><?php echo get_field('the_secon_block_header'); ?></h2>
	</section>

	<section class="price">
		<img src="<?php echo get_template_directory_uri() ?>/assets/img/classes/child_2.png" alt="Tumblebuspdx">

		<div class="content">
			<?php echo get_field('the_second_block_content'); ?>
			<a href="<?php echo get_permalink(11) ?>" class="btn-purple"><span>Sign Up</span></a>
		</div>
	</section>

	<section class="want-party">
		<h2><?php echo get_field('the_third_block_header'); ?></h2>
		<p><?php echo get_field('the_third_block_content'); ?></p>
		<a href="<?php echo get_permalink(11) ?>" class="btn-purple"><span>Sign Up</span></a>
	</section>
</div><!-- #primary -->


<?php
// get_sidebar();
get_footer();
