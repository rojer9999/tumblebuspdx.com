<?php /* Template Name: Contact us page */
get_header();
?>

<div id="primary" class="content-area">

	<section class="content form-section">
		<h1>Contact us</h1>

		<img class="child_1" src="<?php echo get_template_directory_uri() ?>/assets/img/contact/child_2.png" alt="Tumblebus">
		<img class="child_2" src="<?php echo get_template_directory_uri() ?>/assets/img/contact/child_3.png" alt="Tumblebus">

		<div class="form-wrap">
			<?php echo do_shortcode('[contact-form-7 id="45" title="Main form"]'); ?>
			<p class="underform">All classes canceled due to inclement weather, holidays, or special occasions will be made up or refunded. Any activity involving motion or height can be dangerous and may result in injury. There is an inherent risk in this or any other athletic activity. Tumblebus, PDX LLC, Its owners, officers, employees and volunteers shall not be held responsible for any injury to your child as a result of Tumble bus PDX LLC activities. Still photos and video may be taken of children on Tumble bus PDX LLC for the purposes of training, safety, sharing with parents, and publicly.</p>
		</div>
	</section>

</div><!-- #primary -->


<?php
// get_sidebar();
get_footer();
