<?php /* Template Name: Main(Info) page */
get_header();
?>

<div id="primary" class="content-area main-page">

	<section class="bnr-main">
		<div class="wrap">
			<h1><?php bloginfo('name'); ?></h1>
			<p class="description"><?php bloginfo('description'); ?></p>
			<a href="<?php echo get_permalink(11) ?>" class="btn-purple"><span>Sign Up</span></a>
		</div>
	</section>

	<section class="what-is-tmb">
		<div class="wrap">
			<h2><?php echo get_field('main_page_header_first'); ?></h2>
			<p><?php echo get_field('main_page_desc_first'); ?></p>
		</div>
	</section>

	<section class="what-inside">
		<img src="<?php echo get_template_directory_uri() ?>/assets/img/main-page/what-inside-bg.jpg" alt="Tumblebuspdx">
		<div class="wrap">
			<h2><?php echo get_field('main_page_header_second'); ?></h2>
			<p><?php echo get_field('main_page_desc_second'); ?></p>
		</div>
	</section>

	<section class="gallery">
		<h2>Our Gallery</h2>
		<?php echo do_shortcode("[PFG id=31]"); ?>
	</section>

</div><!-- #primary -->


<?php
// get_sidebar();
get_footer();
