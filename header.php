<?php

/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WDD
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">


		<header id="masthead" class="site-header">
			<a class="header-logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/tumblebus_pdx-yellow.png" alt="logo" />
			</a>

			<button class="toggleMenu"><i class="fas fa-bars"></i></button>

			<nav id="site-navigation" class="main-navigation">
				<?php
				wp_nav_menu(array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				));
				?>
			</nav><!-- #site-navigation -->

		</header><!-- #masthead -->