<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package wdd_theme
 */

get_header();
?>

<div id="primary" class="content-area page-404">
	<main id="main" class="site-main">

		<section class="content">
			<div class="container">
				<h1>
					404<BR />
					Page Not Found
				</h1>
			</div>
		</section>


	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
