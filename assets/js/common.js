


// $(document).ready(() => {
jQuery(document).ready(function ($) {
	// Menu mobile
	$('.toggleMenu').click(() => {
		$('.main-navigation').slideToggle(200);
	});
		
	// Service page tabs
	$('.tabs-nav a').click(function (e) {
		e.preventDefault();

		$('.tab').removeClass('active');
		$('.tabs-nav li').removeClass('active');

		$(this).parent('li').addClass('active');

		const href = $(this).attr('href');
		$(href).addClass('active');
	})
})